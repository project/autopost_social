CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Autopost social is a module to publish posts to multi Social network (Facebook, Twitter) when you add/edit a new content.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/autopost_social

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/autopost_social

REQUIREMENTS
------------

This module requires the following SDK:

 * composer require facebook/graph-sdk
 * composer require abraham/twitteroauth

INSTALLATION
------------

 * Enable Module Autopost social.
 * You need to install composer :
        composer require facebook/graph-sdk
        composer require abraham/twitteroauth
 * Visit this page to add your configuration (ClientId, SecretId ...) : admin/structure/autopost-social/settings
 * Enable autopost functionality to content type you want : admin/structure/types/manage/CONTENT-TYPE
 * When you add a new Content you can enable autopost to each social network (facebook, twitter)


CONFIGURATION
-------------

 * Configure your module to add your Clientid and Client secret ... in this page : "admin/structure/autopost-social/settings":
    - You can generate automaticly access token for facebook in the same page : "admin/structure/autopost-social/settings".

MAINTAINERS
-----------

Current maintainers:
 * Zakaria Elhariri (zakaria340) - https://www.drupal.org/u/zakaria340
