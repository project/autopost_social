<?php

namespace Drupal\autopost_social;

use \Drupal\node\NodeInterface;

/**
 * Abstract SocialPostBase class.
 */
abstract class SocialPostBase implements SocialPostInterface {

  /**
   * {@inheritdoc}
   */
  abstract public function post(NodeInterface $node);

  /**
   * {@inheritdoc}
   */
  abstract public function getFormConfig();

}
