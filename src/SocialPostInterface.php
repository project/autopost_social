<?php

namespace Drupal\autopost_social;

use \Drupal\node\NodeInterface;

/**
 * Interface SocialPostInterface.
 *
 * @package Drupal\autopost_social
 */
interface SocialPostInterface {

  /**
   * Post function.
   *
   * @param \Drupal\node\NodeInterface $node
   *    Node.
   */
  public function post(NodeInterface $node);

  /**
   * Get form config.
   */
  public function getFormConfig();

}
