<?php

namespace Drupal\autopost_social\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\autopost_social\SocialPostFactory;

/**
 * Form controller for the autopost_social entity edit forms.
 */
class AutoPostSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'autpost_social_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'autopost_social.settings',
    ];
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   *
   * Builds the entity add/edit form.
   *
   * @param array $form
   *   Associative array containing the structure of the form.
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An associative array containing the Autopost social add/edit form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $providers = \Drupal::config('autopost_social.settings')->get('providers');
    $form['#tree'] = TRUE;
    foreach ($providers as $key => $provider) {
      $providerInstance = SocialPostFactory::create($key);
      $form['provider_' . $key] = $providerInstance->getFormConfig();
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit Form configurations.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $providers = \Drupal::config('autopost_social.settings')->get('providers');
    foreach ($providers as $key => $provider) {
      $this->config('autopost_social.settings')
        ->set('provider_' . $key, $form_state->getValue('provider_' . $key))
        ->save();
    }
    parent::submitForm($form, $form_state);
  }

}
