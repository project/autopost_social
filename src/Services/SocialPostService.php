<?php

namespace Drupal\autopost_social\Services;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use \Drupal\node\NodeInterface;
use Drupal\autopost_social\SocialPostFactory;
use Drupal\autopost_social\Event\AutopostEvents;
use Drupal\autopost_social\Event\AutopostSocialEvent;

/**
 * Service SocialPost.
 */
class SocialPostService implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AutopostEvents::POST_SOCIAL] = ['notifyPost'];
    return $events;
  }

  /**
   * Notify Post function.
   *
   * @param AutopostSocialEvent $event
   *    Autopost social event.
   */
  public function notifyPost(AutopostSocialEvent $event) {
    $this->post($event->getProviders(), $event->getNode());
  }

  /**
   * Post Service.
   *
   * @param array $providers
   *    List providers.
   * @param \Drupal\node\NodeInterface $node
   *    Node to post.
   */
  public function post($providers, NodeInterface $node) {
    foreach ($providers as $provider) {
      $postInstance = SocialPostFactory::create($provider);
      if (!is_null($postInstance)) {
        if ($postInstance->post($node)) {
          drupal_set_message(t('Title published with success'), 'status');
        }
      }
    }
  }

}
