<?php

namespace Drupal\autopost_social\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\autopost_social\SocialPostFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Routing\TrustedRedirectResponse;

use Drupal\autopost_social\SocialPostBase;
use \Drupal\Core\Url;

/**
 * Controller AccessToken.
 *
 * @see /admin/structure/autopost-social/settings.
 */
class GenerateAccessTokenController extends ControllerBase {

  /**
   * Generate token.
   *
   * @param string $provider
   *    Provider name.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *    Redirect to callback url.
   */
  public function generate($provider = NULL) {
    if (!is_null($provider)) {
      $config = Drupal::config('autopost_social.settings')->get(
        'provider_' . $provider
      );
      if (empty($config) || empty($config['client_id'])
        || empty($config['secret_id'])
      ) {
        drupal_set_message(
          t('Client Id and Secret Id are required to generate Access token'),
          'error'
        );
        $configUrl = Url::fromRoute(
          'autopost_social.settings'
        )->toString();
        return new RedirectResponse($configUrl);
      }

      $postInstance = SocialPostFactory::create($provider);
      if (!$postInstance instanceof SocialPostBase) {
        drupal_set_message(
          t('An error occurred when trying to generate access token.'),
          'error'
        );
        $configUrl = Url::fromRoute(
          'autopost_social.settings'
        )->toString();
        return new RedirectResponse($configUrl);
      }
      // We have code we can generate access token.
      if (isset($_GET['code']) && $_GET['code']) {
        $result = $postInstance->generateAccessToken($_GET['code']);
        if (isset($result['error'])) {
          drupal_set_message(
            t('An error occurred when trying to generate access token.'),
            'error'
          );
        }
        else {
          drupal_set_message(
            $result['message'], 'status'
          );
        }
        $configUrl = Url::fromRoute(
          'autopost_social.settings',
          ['absolute' => TRUE]
        )->toString();
        return new RedirectResponse($configUrl);
      }
      else {
        // Get autorization.
        $urlresponse = $postInstance->generateAccessToken();
        return new TrustedRedirectResponse($urlresponse);
      }
    }
    else {
      drupal_set_message(
        t("Param token don't exist"),
        'error'
      );
      return FALSE;
    }
  }

}
