<?php

namespace Drupal\autopost_social\Event;

use Symfony\Component\EventDispatcher\Event;
use \Drupal\node\NodeInterface;

/**
 * Wraps a autopost event for event subscribers.
 *
 * @ingroup autopost_social
 */
class AutopostSocialEvent extends Event {

  /**
   * Incident type.
   *
   * @var string
   */
  protected $providers;

  /**
   * Detailed incident report.
   *
   * @var string
   */
  protected $node;

  /**
   * Constructs an incident report event object.
   *
   * @param array $providers
   *   List providers.
   * @param NodeInterface $node
   *   Node to post.
   */
  public function __construct(array $providers, NodeInterface $node) {
    $this->providers = $providers;
    $this->node = $node;
  }

  /**
   * Get Providers.
   *
   * @return string
   *   The type of report.
   */
  public function getProviders() {
    return $this->providers;
  }

  /**
   * Get Node.
   *
   * @return NodeInterface
   *   Node to post.
   */
  public function getNode() {
    return $this->node;
  }

}
