<?php

namespace Drupal\autopost_social\Event;

/**
 * Defines events for the autopost_social module.
 *
 * @see \Drupal\Core\Config\ConfigCrudEvent
 *
 * @ingroup autopost_social
 */
final class AutopostEvents {

  /**
   * Name of the event fired when we want to post in social media.
   *
   * @Event
   *
   * @see \Drupal\autopost_social\Event\AutopostSocialEvent
   *
   * @var string
   */
  const POST_SOCIAL = 'autopost_social.post_social';

}
