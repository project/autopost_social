<?php

namespace Drupal\autopost_social\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Autopostsocial entity.
 *
 * @ConfigEntityType(
 *   id = "autopostsocial",
 *   label = @Translation("Autopost"),
 *   admin_permission = "administer autopost_social",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   }
 * )
 */
class Autopostsocial extends ConfigEntityBase {

  /**
   * The Autopost ID.
   *
   * @var string
   */
  public $id;

  /**
   * NID.
   *
   * @var string
   */
  public $nid;

  /**
   * The Providers label.
   *
   * @var string
   */
  public $providers;

  /**
   * The Providers status.
   *
   * @var string
   */
  public $status;
}
