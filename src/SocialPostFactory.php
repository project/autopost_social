<?php

namespace Drupal\autopost_social;


/**
 * Factory Class to return Provider class from conf.
 */
class SocialPostFactory {

  /**
   * {@inheritdoc}
   */
  public static function create($provider) {
    $providers = \Drupal::config('autopost_social.settings')->get('providers');

    if (isset($providers[$provider])) {
      $class = $providers[$provider]['class'];
      $values = \Drupal::config('autopost_social.settings')->get(
        'provider_' . $provider
      );
      $socialPost = new $class($values);
      if ($socialPost instanceof SocialPostInterface) {
        return $socialPost;
      }
    }
    return NULL;
  }

}
