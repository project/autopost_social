<?php

namespace Drupal\autopost_social\Providers\Socialpost;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Drupal\autopost_social\SocialPostBase;
use \Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Facebook Provider Class.
 */
class FacebookSocialPost extends SocialPostBase {

  /**
   * Client id App.
   *
   * @var string clientid.
   */
  protected $clientId;

  /**
   * Page name.
   *
   * @var string name.
   */
  protected $pagename;

  /**
   * Secret id App.
   *
   * @var string secretId.
   */
  protected $secretId;

  /**
   * Access token app.
   *
   * @var string $accessToken .
   */
  protected $accessToken;

  /**
   * FacebookSocialPost constructor.
   *
   * @param array $valuesFacebook
   *    Values facebook.
   */
  public function __construct(array $valuesFacebook) {
    if (!empty($valuesFacebook)) {
      $this->setClientId($valuesFacebook['client_id']);
      $this->setSecretId($valuesFacebook['secret_id']);
      $this->setPageName($valuesFacebook['page_name']);
      $this->setAccessToken($valuesFacebook['access_token']);
    }
  }

  /**
   * Function to Post to facebook.
   *
   * @param \Drupal\node\NodeInterface $node
   *    Node To post.
   *
   * @return bool
   *    Return true|false.
   */
  public function post(NodeInterface $node) {

    try {
      $fb = new Facebook(
            [
              'app_id'               => $this->getClientId(),
              'app_secret'           => $this->getSecretId(),
              'default_accessToken' => $this->getAccessToken(),
            ]
        );
      $fb->post(
            '/' . $this->getPageName() . '/feed/', array(
              'access_token' => $this->getAccessToken(),
              'message' => $node->getTitle(),
              'link' => Url::fromRoute('entity.node.canonical', ['node' => $node->id()], ['absolute' => TRUE])->toString(),
            )
        );
      return TRUE;
    }
    catch (FacebookSDKException $e) {
      \Drupal::logger('autopost_social')->error(
            'Graph returned an error: ' . $e->getMessage()
        );
      return FALSE;
    }
  }

  /**
   * Get custom config (ClientId, SecretId ..)
   *
   * @return array
   *    return config.
   */
  public function getFormConfig() {

    $key = 'facebook';
    $form = [
      '#type'   => 'fieldset',
      '#title'  => 'Facebook',
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => t('Client Id'),
      '#default_value' => $this->getClientId(),
      '#description' => t(
        'You need to create an app to have client id => %urlCreateApp.', [
          '%urlCreateApp' => 'https://developers.facebook.com/docs/apps/register',
        ]
      ),
    ];

    $form['secret_id'] = [
      '#type' => 'textfield',
      '#title' => t('Secret Id'),
      '#default_value' => $this->getSecretId(),
      '#description' => t(
        'You need to create an app to have client id => %urlCreateApp.', [
          '%urlCreateApp' => 'https://developers.facebook.com/docs/apps/register',
        ]
      ),
    ];

    $form['page_name'] = [
      '#type'          => 'textfield',
      '#title'         => t('Machine Page Name'),
      '#default_value' => $this->getPageName(),
      '#description'   => t('Page name you want to Autopost.'),
    ];
    $url = Url::fromRoute(
          'autopost_social.access_token', ['provider' => $key], ['absolute' => TRUE]
      );

    $urlGenerateAccessToken = Link::fromTextAndUrl(
          t(
              'Click here to generate Access Token For : %', ['%' => 'Facebook']
          ), $url
      );

    $form['access_token'] = [
      '#type'          => 'textarea',
      '#title'         => t('Access token'),
      '#description'   => $urlGenerateAccessToken,
      '#default_value' => $this->getAccessToken(),
    ];
    return $form;
  }

  /**
   * Generate Access Token.
   *
   * @param string $code
   *    Code.
   *
   * @return array|string
   *    Long access token value.
   */
  public function generateAccessToken($code = NULL) {

    $fb = new Facebook(
          [
            'app_id'                => $this->getClientId(),
            'app_secret'            => $this->getSecretId(),
            'default_graph_version' => 'v2.5',
          ]
      );

    if (!is_null($code)) {
      try {
        $helper = $fb->getRedirectLoginHelper();
        $accessToken = $helper->getAccessToken();

        $longLivedToken = $fb->getOAuth2Client()->getLongLivedAccessToken(
              $accessToken
          );
        $fb->setDefaultAccessToken($longLivedToken);
        $response = $fb->sendRequest(
              'GET', $this->getPageName(), ['fields' => 'access_token']
          )->getDecodedBody();
        $foreverPageAccessToken = $response['access_token'];

        $facebook_config = \Drupal::config('autopost_social.settings')->get(
              'provider_facebook'
          );
        $facebook_config['access_token'] = $foreverPageAccessToken;

        \Drupal::configFactory()->getEditable('autopost_social.settings')->set(
              'provider_facebook', $facebook_config
          )->save();
        return array('message' => 'Access token generated with success.');
      }
      catch (FacebookResponseException $e) {
        // When Graph returns an error.
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      }
      catch (FacebookSDKException $e) {
        // When validation fails or other local issues.
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
    }
    else {
      $helper = $fb->getRedirectLoginHelper();
      $permissions = ['manage_pages', 'publish_pages'];
      $urlcallback = Url::fromRoute(
            'autopost_social.access_token', ['provider' => 'facebook'],
            ['absolute' => TRUE]
        )->toString(TRUE)->getGeneratedUrl();
      $loginUrl = $helper->getLoginUrl($urlcallback, $permissions);
      return $loginUrl;
    }

  }

  /**
   * Set clientId.
   *
   * @param string $clientId
   *   ClientId.
   */
  public function setClientId($clientId) {
    $this->clientId = $clientId;
  }

  /**
   * Get clientId.
   *
   * @return mixed
   *    ClientId value.
   */
  public function getClientId() {
    return $this->clientId;
  }

  /**
   * Get Pagename.
   *
   * @param string $pagename
   *    PageName.
   */
  public function setPageName($pagename) {
    $this->pagename = $pagename;
  }

  /**
   * Get Pagename.
   *
   * @return string
   *    Pagename
   */
  public function getPageName() {
    return $this->pagename;
  }

  /**
   * Set secretId.
   *
   * @param string $secretId
   *   Secretid.
   */
  public function setSecretId($secretId) {
    $this->secretId = $secretId;
  }

  /**
   * Get secretId.
   *
   * @return string
   *    Secretid.
   */
  public function getSecretId() {
    return $this->secretId;
  }

  /**
   * Set access Token.
   *
   * @param string $accessToken
   *   Access token.
   */
  public function setAccessToken($accessToken) {
    $this->accessToken = $accessToken;
  }

  /**
   * Get Access token.
   *
   * @return \Drupal\autopost_social\Providers\Socialpost\FacebookSocialPost
   *   Access token Value.
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

}
