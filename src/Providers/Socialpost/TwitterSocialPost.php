<?php

namespace Drupal\autopost_social\Providers\Socialpost;

use Drupal\Component\Utility\Unicode;
use Drupal\autopost_social\SocialPostBase;
use \Drupal\node\NodeInterface;
use Abraham\TwitterOAuth\TwitterOAuth;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Twitter Provider Class.
 */
class TwitterSocialPost extends SocialPostBase {


  /**
   * Client id App.
   *
   * @var string clientid.
   */
  protected $clientId;

  /**
   * Secret id App.
   *
   * @var string secretId.
   */
  protected $secretId;

  /**
   * Access token app.
   *
   * @var string $accessToken .
   */
  protected $accessToken;

  /**
   * Access token secret.
   *
   * @var string $accessToken_secret .
   */
  protected $accessTokenSecret;

  /**
   * TwitterSocialPost constructor.
   *
   * @param array $valuesTwitter
   *    Values twitter conf.
   */
  public function __construct(array $valuesTwitter) {
    if (!empty($valuesTwitter)) {
      $this->setClientId($valuesTwitter['client_id']);
      $this->setSecretId($valuesTwitter['secret_id']);
      $this->setAccessToken($valuesTwitter['access_token']);
      $this->setAccessTokenSecret($valuesTwitter['access_token_secret']);
    }
  }

  /**
   * Function to Post to twitter.
   *
   * @param NodeInterface $node
   *    Node To post.
   *
   * @return bool
   *    Return true|false.
   */
  public function post(NodeInterface $node) {

    $connection = new TwitterOAuth(
      $this->getClientId(), $this->getSecretId(), $this->getAccessToken(), $this->getAccessTokenSecret()
    );

    $msgToTweet = $node->getTitle();
    $msgToTweet = Unicode::truncate($msgToTweet, 140 - 25, TRUE, TRUE);
    $msgToTweet .= ' ' . Url::fromRoute('entity.node.canonical', ['node' => $node->id()], ['absolute' => TRUE])->toString();
    $statues = $connection->post("statuses/update", ["status" => $msgToTweet]);
    if (isset($statues->errors)) {
      $error_message = $statues->errors[0]->message;
      \Drupal::logger('autopost_social')->error(
        'Twitter returned an error: ' . $error_message
      );
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get custom config (ClientId, SecretId ..)
   *
   * @return array
   *    return config.
   */
  public function getFormConfig() {
    $form = [
      '#type'   => 'fieldset',
      '#title'  => 'Twitter',
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['client_id'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Consumer Key (API Key)'),
      '#default_value' => $this->getClientId(),
      '#description'   => t(
        'You need to create an app to have client id => %urlCreateApp.', array(
          '%urlCreateApp' => "https://apps.twitter.com/app/new",
        )
      ),
    );

    $form['secret_id'] = array(
      '#type'          => 'textfield',
      '#title'         => 'Consumer Secret (API Secret)',
      '#default_value' => $this->getSecretId(),
      '#description'   => t(
        'You need to create an app to have client id => %urlCreateApp.', array(
          '%urlCreateApp' => "https://apps.twitter.com/app/new",
        )
      ),
    );

    $link = Link::fromTextAndUrl(
      t('Click here to get Access Token For : %', array('%' => 'Twitter')), Url::fromUri('https://apps.twitter.com/app')
    )->toString();
    $form['access_token'] = array(
      '#type'          => 'textarea',
      '#title'         => 'Access token',
      '#description'   => $link,
      '#default_value' => $this->getAccessToken(),
    );

    $form['access_token_secret'] = array(
      '#type'          => 'textarea',
      '#title'         => 'Access token Secret',
      '#description'   => $link,
      '#default_value' => $this->getAccessTokenSecret(),
    );
    return $form;
  }

  /**
   * Set clientId.
   *
   * @param string $clientId
   *   ClientId.
   */
  public function setClientId($clientId) {
    $this->clientId = $clientId;
  }

  /**
   * Get clientId.
   *
   * @return mixed
   *    ClientId value.
   */
  public function getClientId() {
    return $this->clientId;
  }

  /**
   * Set secretId.
   *
   * @param string $secretId
   *   Secretid.
   */
  public function setSecretId($secretId) {
    $this->secretId = $secretId;
  }

  /**
   * Get secretId.
   *
   * @return string
   *    Secretid.
   */
  public function getSecretId() {
    return $this->secretId;
  }

  /**
   * Set access Token.
   *
   * @param string $accessToken
   *   Access token.
   */
  public function setAccessToken($accessToken) {
    $this->accessToken = $accessToken;
  }

  /**
   * Get Access token.
   *
   * @return \Drupal\autopost_social\Providers\Socialpost\TwitterSocialPost
   *   Access token Value.
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

  /**
   * Set access token secret.
   *
   * @param string $accessTokenSecret
   *   Access token secret.
   */
  public function setAccessTokenSecret($accessTokenSecret) {
    $this->accessTokenSecret = $accessTokenSecret;
  }

  /**
   * Get Access token secret.
   *
   * @return \Drupal\autopost_social\Providers\Socialpost\TwitterSocialPost
   *    Access token secret.
   */
  public function getAccessTokenSecret() {
    return $this->accessTokenSecret;
  }

}
